.data
str:	.ascii	"The sum of 1 to 10 is\n"

.text
	li	$t0, 0		# sum
	li	$t1, 1		# counter
	
main:
	bgt	$t1, 10, exit	# loop main while counter is smaller than 10
	add	$t0, $t0, $t1	# sum = sum + counter
	add	$t1, $t1, 1	# counter = counter + 1
	j	main

exit:
	la	$a0, str	# load address of str
	li	$v0, 4
	syscall			# print string
	
	la	$a0, ($t0)	# load address of sum
	li	$v0, 1
	syscall			# print sum

	li	$v0, 10
	syscall			# exit