# mips3.asm - FizzBuzz
.data
strsp:	.asciiz	" \0"
strfz:	.asciiz	"fizz\0"
strbz:	.asciiz	"buzz\0"
strnl:	.asciiz	"\n\0"


.text
	li	$t0, 0		# counter
	
	li	$v0, 5		# input
	syscall
	la	$t1, ($v0)	# load address of input
	
	li	$t2, 3
	li	$t3, 5

main:
	add	$t0, $t0, 1
	bgt	$t0, $t1, exit	# loop main while counter is smaller than input
	
	# print counter and space
	la	$a0, ($t0)	# load address of counter
	li	$v0, 1
	syscall			# print counter
	la	$a0, strsp	# load address of strsp
	li	$v0, 4
	syscall			# print string
	
	# when 3 is a factor of counter
	div	$t0, $t2
	mfhi	$t4		# counter % 3
	beq	$t4, $zero, case1

	# when 5 is a factor of counter
	div	$t0, $t3
	mfhi	$t4		# counter % 5
	beq	$t4, $zero, case2
	
	# new line
	la	$a0, strnl	# load address of strnl
	li	$v0, 4
	syscall			# print string
	
	j	main		# jump to main
	
case1:
	# print fizz
	la	$a0, strfz	# load address of strfz
	li	$v0, 4
	syscall			# print string
	
	# when 5 is also a factor of counter
	div	$t0, $t3
	mfhi	$t4		# counter % 5
	beq	$t4, $zero, case2
	
	# new line
	la	$a0, strnl	# load address of strnl
	li	$v0, 4
	syscall			# print string
	
	j	main		# jump to main

case2:
	# print buzz
	la	$a0, strbz	# load address of strbz
	li	$v0, 4
	syscall			# print string

	# new line
	la	$a0, strnl	# load address of strnl
	li	$v0, 4
	syscall			# print string
	
	j	main		# jump to main
	
exit:
	li	$v0, 10
	syscall			# exit