.data
str:	.ascii	"The sum total is\n"

.text
	li	$t0, 0		# sum
	li	$t1, 1		# counter

	li	$v0, 5		# input
	syscall
	la	$t2, ($v0)	# load address of input

main:
	bgt	$t1, $t2, exit	# loop main while counter is smaller than input
	add	$t0, $t0, $t1	# sum = sum + counter
	add	$t1, $t1, 1	# counter = counter + 1
	j	main

exit:
	la	$a0, str	# load address of str
	li	$v0, 4
	syscall			# print string
	
	la	$a0, ($t0)	# load address of sum
	li	$v0, 1
	syscall			# print sum

	li	$v0, 10
	syscall			# exit